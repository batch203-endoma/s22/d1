// Array Methods

/*

-javascript has built-in function and methods for arrays. this allows us to manipulate and access array items.

-Array can be either mutated or iterated

  -Array "mutation" seek to modify the contents of an array while array iteration aim to evaluate and look over each element.

//Push ()

-adds an element in the end of an array and returns the new array's length

- Syntax:
arrayName.push(newElement);



*/

let fruits = ["Apple", "Orange", "Kiwi", "Dragon Fruit"]

console.log("Current Array: ");
console.log(fruits);
let fruitsLength = fruits.push("Mango");
console.log(fruitsLength);
console.log("Mutated array from push method: ");
console.log(fruits);


fruits.push("Avocado", "Guava");
console.log("Mutated array from push method: ");
console.log(fruits);


//pop()


/*
Syntax
  -Removes the last element in an array and returns the removed element.

  -Syntax:
    arrayName.pop();
*/
let removedFruit = fruits.pop(); //returns removed element
console.log(removedFruit);
console.log("Mutated array from push method: ");
console.log(fruits);

//unshift()
// add one or more elements at the beginning of an array

fruits.unshift("Lime", "Banana")
console.log("Mutated array from push method: ");
console.log(fruits);


// Shift - removes 1 from the beginning.

let anotherFruit = fruits.shift();
console.log(anotherFruit);
console.log("Mutated array from push method: ");
console.log(fruits);


//splice()
/*

  -simultaneously removes an element from a specified index number and adds new elements.
  -syntax:
    arrayName.splice(startingIndex, deletecount, elementsToBeAdded)


*/

fruits.splice(1, 2, "lime", "Cherry");
console.log("Mutated array from push method: ");
console.log(fruits);



//Sort()

/*
-rearranges the array elements in alphanumeric order

Syntax
  -arrayName.sort();

*/

fruits.sort();
console.log("Mutated array from sort method: ");
console.log(fruits)

//Reverse - reverse of sort

fruits.reverse();
console.log("Mutated array from sort method: ");
console.log(fruits)


// non-mutator methods
/*

  -Non-mutator methods are function that do not modify or change an array after they're crated.
  -


*/
// indexOf()

/*
  -returns the index of the first matching element found in an array.
  -If no match was found, the result will be -1.
  The search process will be done from the first element proceeding to the last element.


    syntax
        arrayName.indexOf(searchValue);
        arrayName.indexOf(searchValue, fromIndex)<<Starting
*/
let countries = ["US", "PH", "CAN", "SG", "TH", "PH", "FR", "DE"];


let firstIndex = countries.indexOf("PH", 2);
console.log("Resulft of indexOF method: " + firstIndex);

let invalidCountry = countries.indexOf("BR");
console.log("Resulft of indexOF method: " + invalidCountry);


// LastIndexOf()

/*
      -Returns the index number of the last matching element found in an array
      -The search from process will be done from the last element proceeding to the first element.
      -syntax
        arrayName.LastIndexOf(SearchValue);
        arrayName.LastiNDEXoF(searchValue, fromIndex);
        end to start <------
  */

let lastIndex = countries.lastIndexOf("PH");
console.log("Result of LastIndexOf method: " + lastIndex);

let lastIndexStart = countries.lastIndexOf("PH", 4);
console.log("Result of LastIndexOf method: " + lastIndexStart);

// slice()

/*

    -Portions/slices element form an array AND returns a new array.

    -Syntax:
      arrayName.slice(startingIndex); //until the last element of the array

      arrayName.slice(startingIndex, endingIndex);

*/

console.log("Results before Slice");
console.log(countries);
let sliceArrayA = countries.slice(2);
console.log("Result from slice method: ");
console.log(sliceArrayA);

let sliceArrayB = countries.slice(2, 4);
console.log("Result from slice method: ");
console.log(sliceArrayB);

// Hindi kasama ung ending. pero ung starting kasama.

let sliceArrayC = countries.slice(-3);
console.log("Result from slice method: ");
console.log(sliceArrayC);



// toString()
/*

  -Returns an array as a string, separated by commas.
  -Syntax
    arrayName.toString()


*/


let stringArray = countries.toString();
console.log("Result from toString method: ");
console.log(stringArray);


// concat()

/*

  -combines two arrays and returns the combined resut.
  -syntax:
    arrayA.concat(arrayB);
    arrayA.concat(element);

  */


let tasksArrayA = ["drink html", "eat javascript"];
let tasksArrayB = ["inhale css", "breathe sass"];
let tasksArrayC = ["get git", "be node"];

let tasks = tasksArrayA.concat(tasksArrayB);
console.log("Result from concat method: ");
console.log(tasks);


//Combining multiple arrays

console.log("Result from concat Method: ")
let allTasks = tasksArrayA.concat(tasksArrayB, tasksArrayC);
console.log(allTasks);

//combine array with elements

let combinedTasks = tasksArrayA.concat("smell express", "throw react");
console.log("Result from concat Method: ");
console.log(combinedTasks);


//join()

/*
  -returns an array as a string separated by specified by specified separator string.
  - syntax
    arrayName.join("separatorString");

*/

let users = ["John", "Jane", "Joe", "Juan"];
console.log(users.join("  "));
console.log(users.join());

//Iteration methods
/*
  -Iteration methods are loops designed to perform repetitive tasks on arrays.
  -it loops over all items/elements in an array.

*/

//forEach()


/*
  -Similar to a for loop that iterates on each array elements

  -syntax:
    arrayName.forEach(function(indivElement)){
    //statement/code block.
  })
//Does not return anything.

  for let i=0; i<allTask.length; i++){
  console.log(allTask[i])
}


*/


//using forEach with conditional statement.

let filteredTasks = []; // store in this variables all the task with caracters greater than 10.


allTasks.forEach(function(task) {
  // //It is a good practice to print the current element in the array console when working with array methods to have an idea of what information is being worked on for each iteration.
  // console.log(task);

  if (task.length > 10) {
    filteredTasks.push(task);
    console.log(filteredTasks);

  }
})


// map()
//-iterates on each element and returns new array with different values depending on the result of the function's operation.

//-this is useful for performing tasks where mutating/chaging the elements required.

//-Syntax let/const resultArray = arrayName.map(function(indivElement));

let numbers = [1, 2, 3, 4, 5];

//return squared values of each element
let numbersMap = numbers.map(function(number) {
  return number * number;
})

console.log("Original array: ");
console.log(numbers); //original is not affected by map();

console.log("Result of the map method: ");
console.log(numbersMap);

let numberForEach = numbers.forEach(function(number) {
  console.log(number * number);
  return number * number;
});

console.log(numberForEach); // undefined.
// forEach(), loops over all items in the array as does map, but forEach() does not return a new array.


//every -boolean
// -checks if all elements in an array meet the given condition
// - This is useful for validation data stored in arrays escpecially when dealing with large amounts of data. (e.g. Fasfood chain branches)
/*
  -Syntax:
    let/const = resultArray = arrayName.every(function(individual element){
    return expression/condition;
  })

*/


numbers = [5, 7, 9, 8, 5];
let allValid = numbers.every(function(number) {
  return (number < 3);
});

console.log("Result of every method: ");
console.log(allValid);



//some() -boolean

/*
-syntax:
let/const resultArray = arrayName.some(function(individElement){
return expression/condition
})


*/


let someValid = numbers.some(function(number) {
  return (number < 2);
})

console.log("Result of some Method: ");
console.log(someValid);

numbers = [1, 2, 3, 4, 5];

// filter -returns new array

//return on those valid based on the statement/condition given.

let filterValid = numbers.filter(function(number) {
  return (number < 3);
})

console.log("Result of filter method: ");
console.log(filterValid);


let nothingFound = numbers.filter(function(number) {
  return number == 0;
})

console.log("Result of filter Method: ");
console.log(nothingFound);

// Shorten the Syntax
filteredTasks = allTasks.filter(function(task) {
  return (task.length > 10);
})

console.log("Result of filtered task using filter method: ");
console.log(filteredTasks);


// includes() - Boolean

/*

  -checks if the argument passed can be found in the array.
  -it returns a boolean whicn ca be saved in a variable.

  syntax:
    let/const variableName=
    arrayName.include (<argumentToFind>);
*/

let products = ["Mouse", "Keyboard", "Laptop", "Monitor"];

let productFound1 = products.includes("Mouse");
console.log("Result of includes method: ");
console.log(productFound1);

let productNotFound = products.includes("Headset");
console.log("Result of includes method: ");
console.log(productNotFound);

// Method chaining
// The resulf ot the first method is used in the second method until all "chained" methods have been resolved.

let filteredProducts = products.filter(function(product) {
  return product.toLowerCase().includes("a");
})

console.log("Result of the chained Method: ");
console.log(filteredProducts);


// reduce()
/*
  - Evaluates elements from left and right and returns/reduces the array into a single value.
  - Syntax:
   let/const resultVariable = arrayName.reduce(function(accumulator, currentvalue){
   return expression/operation
 });
    -accumulator parameter stores the result for every loop.
    -currentValue parameter refers to the current/next element in the array that is evaluated in each iteration of the loop.



*/

let i = 0;

numebrs = [1, 2, 3, 4, 5];

let reducedArray = numbers.reduce(function(acc, cur) {
  console.warn("current iteration" + ++i);
  console.log("accumulator: " + acc);
  console.log("current value: " + cur);


  // The operation or expression of reduce the array into a single value.

  return acc + cur;

});


/*
  How the "reduce" method works:
  1. The first/result element in the array is sotred in the "acc" parameneter
  2. The second/next element in the array is stored in the "cur" parameter
  3. An operation is performed on two elements.
  4. the loop repeats step 1-e until all elements have been worked.


*/









//
